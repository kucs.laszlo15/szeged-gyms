gyms = Gym.create([
                    {
                      name: "Cedrus Fitness",
                      image_url: "https://cedrusfitness.hu/wp-content/uploads/2020/11/cedrus-premium-fittnes-logo.svg"
                    },
                    {
                      name: "Global Fitness",
                      image_url: "http://szegedfitness.hu/wp-content/uploads/2019/03/logo-header-150-150.png"
                    }
                  ])

reviews = Review.create([
                          {
                            title: "Remek a hely!",
                            description: "Nagyon jol ereztem magam",
                            score: 5,
                            gym: Gym.first
                          },
                          {
                            title: "Nagyon rossz volt :/",
                            description: "Nem ereztem jol magam! ",
                            score: 1,
                            gym: Gym.first
                          }
                        ])