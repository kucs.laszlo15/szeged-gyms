Rails.application.routes.draw do
  root 'pages#index'

  namespace :api do
    namespace :v1 do
      get 'reviews/create'
      get 'reviews/destroy'
      get 'gyms/index'
      get 'gyms/show'
      get 'gyms/create'
      get 'gyms/update'
      get 'gyms/destroy'
      resources :gyms, param: :slug
      resources :reviews, only: [:create, :destroy]
    end
  end

  get '*path', to: 'pages#index', via: :all
end
