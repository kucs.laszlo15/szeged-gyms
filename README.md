# Szeged Gym Finder

This project aims to gather information about gyms in Szeged, Hungary, allowing users to review and rate them. The application is built using React for the frontend and Ruby on Rails for the backend.

## Setup

### Rails Backend

1. **Clone the repository:**
git clone 



2. **Navigate to the backend directory:**
cd backend


3. **Install dependencies:**
bundle install


4. **Set up the database:**
rails db:create
rails db:migrate
rails db:seed


5. **Start the Rails server:**
rails server


### React Frontend

1. **Navigate to the frontend directory:**
cd frontend

2. **Install dependencies:**
npm install


3. **Start the React development server:**
npm start


