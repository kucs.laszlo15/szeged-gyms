import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import App from '../components/App'

document.addEventListener('DOMContentLoaded', () => {
    const root = document.createElement('div');
    document.body.appendChild(root);
    const container = ReactDOM.createRoot(root);
    container.render(
        <Router>
            <Routes>
                <Route path="*" element={<App />} />
            </Routes>
        </Router>
    );
})