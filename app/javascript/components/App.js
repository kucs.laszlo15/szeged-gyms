import React from "react";
import { Route, Routes } from "react-router-dom";
import Gyms from "./Gyms/Gyms";
import Gym from "./Gym/Gym";

function App() {
    return (
        <Routes>
            <Route path="*" element={<Gyms />} />
            <Route path="/gyms/:slug" element={<Gym />} />
        </Routes>
    );
}

export default App;