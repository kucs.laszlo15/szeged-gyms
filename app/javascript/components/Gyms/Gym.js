import React from "react";
import {Link} from "react-router-dom";
import styled from "styled-components";

const Card = styled.div`
  border: 1px solid #EFEFEF;
  background: #FFF;
  text-align: center;
`
const GymLogo = styled.div`
  width: 50px;
  text-align: center;
  margin-right: auto;
  margin-left: auto;
  padding-top: 10px;
  
  img {
    height: 50px;
    width: 50px;
    border-radius: 100%;
    border: 1px solid #EFEFEF;
  }
`
const GymName = styled.div`
  padding: 20px 0 10px 0;
`
const LinkWrapper = styled.div`
  margin: 30px 0 20px 0;
  height: 50px;
  
  a{
    color: #FFF;
    background: #000;
    border-radius: 4px;
    padding: 10px 50px;
    border: 1px solid #000;
    width: 100%;
    text-decoration: none;
  }
`
const Gym = (props) => {
    return (
        <Card>
            <GymLogo>
                <img src={props.attributes.image_url} alt={props.attributes.name}/>
            </GymLogo>
            <GymName>{props.attributes.name}</GymName>
            <div className="gym-score">{props.attributes.avg_score}</div>
            <LinkWrapper>
                <Link to={`/gyms/${props.attributes.slug}`}>View Gym</Link>
            </LinkWrapper>
        </Card>
    )
}

export default Gym