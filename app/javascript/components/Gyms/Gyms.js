import React, { useState, useEffect, Fragment } from 'react';
import axios from 'axios';
import Gym from './Gym';
import styled from "styled-components";

const Home = styled.div`
  text-align: center;
  max-width: 1200px;
  margin-left: auto;
  margin-right: auto;
`
const Header = styled.div`
  padding: 100px 100px 10px 100px;
  
  h1{
    font-size: 42px;
    
  }
`
const Subheader = styled.div`
  font-weight: 300;
  font-size: 26px;
`
const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 20px;
  width: 100%;
  padding: 20px;
`
const Gyms = () => {
    const [gyms, setGyms] = useState([]);

    useEffect(() => {
        axios.get('/api/v1/gyms.json')
            .then(resp => setGyms(resp.data.data))
            .catch(err => console.log(err));
    }, []);

    const grid = gyms.map(item => {
        return (
            <Gym key={item.attributes.name} attributes={item.attributes} />
        );
    });

    return (
        <Home>
            <Header>
                <h1>Szeged Gyms </h1>
                <Subheader>Értékeld Szeged konditermeit őszintén, szívből.</Subheader>
            </Header>
            <Grid>
                {grid}
            </Grid>
        </Home>
    );
}

export default Gyms;