import React from "react";

const Review = (props) => {
    const { avg_score, name, relationships } = props.data.attributes;
    const { data: reviews } = relationships.reviews;

    console.log(relationships.reviews)

    return (
        <div className="review-container">
            <div className="gym-info">
                <div className="gym-name">{name}</div>
                <div className="gym-average-score">{avg_score}</div>
            </div>
            <div className="reviews">
                {reviews.map(review => (
                    <div key={review.id} className="review">
                        <div className="review-score">{review.attributes.score}</div>
                        <div className="review-title">{review.attributes.title}</div>
                        <div className="review-description">{review.attributes.description}</div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Review;