import React, { useState, useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import axios from "axios";
import Header from "./Header";
import styled from "styled-components";
import ReviewForm from "./ReviewForm";
import Review from "./Review";

const Wrapper = styled.div`
  margin-left: auto;
  margin-right: auto;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
`
const Column = styled.div`
  background: #FFF;
  height: 100vh;
  overflow: scroll;

  &:last-child{
    background: #000;
    
  }
`
const Main =  styled.div`
  padding-left: 50px;
    
`


const Gym = () => {
    const { slug } = useParams();
    const [gym, setGym] = useState({});
    const [review, setReview] = useState({ title: '', description: '', score: 0 });
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {

        const url = `/api/v1/gyms/${slug}`;

        axios.get(url)
            .then(resp => {
                setGym(resp.data)
                setLoaded(true)
            })
            .catch(error => {
                console.error('Error fetching gym data:', error);
            });
    }, [slug]);

    const handleChange = (e) => {
        e.preventDefault();

        const { name, value } = e.target;

        setReview(prevReview => ({
            ...prevReview,
            [name]: value
        }));

        console.log('review', review);

    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const csrfToken = document.querySelector('[name=csrf-token]').content;
        axios.defaults.headers.common['X-CSRF-TOKEN'] = csrfToken;
        const gym_id = gym.data.id;
        axios.post('/api/v1/reviews', {review, gym_id})
            .then(resp => {
                const included = [...gym.included, resp.data.data];
                setGym({...gym, included});
                setReview({title: '', description: '', score: 0});
            })
            .catch(resp => {});
    }

    useEffect(() => {
        setReview({title: '', description: '', score: 0});
    }, [gym]);

    const setRating = (score, e) => {
        setReview({...review, score})
    }

    let reviews
    if (loaded && gym.included) {
        reviews = gym.included.map((item, index) => (
            <Review
                key={index}
                data={item} // Átadjuk az adatokat a Review komponensnek
            />
        ));
    }

    return (
        <Wrapper>
            {
                loaded &&
                <Fragment>
                    <Column>
                        <Main>
                            <Header
                                attributes={gym.data.attributes}
                                reviews={gym.data.relationships.reviews}
                            />
                            {reviews}
                        </Main>
                    </Column>
                    <Column>
                        <ReviewForm
                            handleChange={handleChange}
                            handleSubmit={handleSubmit}
                            setRating={setRating}
                            attributes={gym.data.attributes}
                            reviews={review}
                        />
                    </Column>
                </Fragment>
                }
        </Wrapper>
    );
};


export default Gym;