class ReviewSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :description, :score, :gym_id
end
