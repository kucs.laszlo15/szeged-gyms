class Api::V1::GymsController < ApplicationController
  def index
    gyms = Gym.all
    render json: GymSerializer.new(gyms, options).serialized_json
  end

  def show
    gym = Gym.find_by(slug: params[:slug])

    render json: GymSerializer.new(gym).serialized_json
  end

  def create
    gym = Gym.new(gym_params)

    if gym.save
      render json: GymSerializer.new(gym).serialized_json
    else
      render json: {error: gym.errors.messages}, status: 442
    end
  end

  def update
    gym = Gym.find_by(slug: params[:slug])

    if gym.update(gym_params)
      render json: GymSerializer.new(gym).serialized_json
    else
      render json: {error: gym.errors.messages}, status: 442
    end
  end

  def destroy
    gym = Gym.find_by(slug: params[:slug])

    if gym.destroy
      render :no_content
    else
      render json: {error: gym.errors.messages}, status: 442
    end
  end

  private

  def gym_params
    params.require(:gym).permit(:name, :image_url)
  end

  def options
    @options ||= { include: %i[reviews]}
  end
end

