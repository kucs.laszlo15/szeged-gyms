class Api::V1::ReviewsController < ApplicationController
  def create
    review = gym.reviews.new(review_params)

    if review.save
      render json: ReviewSerializer.new(review).serialized_json
    else
      render json: {error: review.errors.messages}, status: 442
    end
  end

  def destroy
    review = Review.find_by(params[:id])

    if review.destroy
      render :no_content
    else
      render json: {error: review.errors.messages}, status: 442
    end
  end


  private

  def gym
    @gym ||= Gym.find(params[:gym_id])
  end

  def review_params
    params.require(:review).permit(:title, :description, :score, :gym_id)
  end
end
