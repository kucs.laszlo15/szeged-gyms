class Gym < ApplicationRecord
  has_many :reviews

  before_create :slugify

  def slugify
    self.slug = name.parameterize
  end

  def avg_score
    return 0 if reviews.empty?

    reviews.average(:score).to_f.round(2)
  end
end
